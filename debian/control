Source: audiocd-kio
Section: kde
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
           Norbert Preining <norbert@preining.info>,
           Pino Toscano <pino@debian.org>,
           Sune Vuorela <sune@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-sequence-kf6,
               cmake (>= 3.16~),
               extra-cmake-modules (>= 6.3.0~),
               gettext,
               libasound2-dev,
               libcdparanoia-dev,
               libflac-dev,
               libkcddb6-dev (>= 4:5.1~),
               libkcompactdisc-dev (>= 4:24.08.2-1~),
               libkf6config-dev (>= 6.3.0~),
               libkf6doctools-dev (>= 6.3.0~),
               libkf6i18n-dev (>= 6.3.0~),
               libkf6kcmutils-dev (>= 6.3.0~),
               libkf6kio-dev (>= 6.3.0~),
               libkf6widgetsaddons-dev (>= 6.3.0~),
               libogg-dev,
               libvorbis-dev,
               qt6-base-dev (>= 6.5.0~),
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: http://www.kde.org/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/audiocd-kio
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/audiocd-kio.git

Package: kio-audiocd
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends},
Recommends: lame, systemsettings,
Description: transparent audio CD access for applications using the KDE Platform
 This package includes the audiocd KIO plugin, which allows applications using
 the KDE Platform to read audio from CDs and automatically convert it into other
 formats.
 .
 This package is part of the KDE multimedia module.

Package: kio-audiocd-dev
Section: devel
Architecture: any
Depends: kio-audiocd (= ${binary:Version}),
         libkcddb6-dev (>= 4:5.1~),
         libkf6kio-dev (>= 6.3.0~),
         qt6-base-dev (>= 6.5.0~),
         ${misc:Depends},
Description: development files for the audio CD KIO plugin
 This package contains development files for building plugins for the audio CD
 KIO plugin.
 .
 This package is part of the KDE multimedia module.
